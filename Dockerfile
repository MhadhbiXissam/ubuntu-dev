FROM ubuntu:22.04
RUN apt-get update
RUN apt-get install -y locales
RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.utf8
RUN export LANG=en_US.utf8



##################################################
#             Configure tzdata                   #
##################################################
# Set keyboard configuration to AZERTY (French)
RUN echo 'fr' > /etc/default/keyboard
# Set locale and language to English (US)
RUN apt-get update && apt-get install -y locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
RUN locale-gen
RUN update-locale LANG=en_US.UTF-8
# Set geographic zone to Europe/Paris
RUN apt-get install -y tzdata
RUN echo 'Europe/Paris' > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata



ENV GIT_USER_NAME ""
ENV GIT_USER_EMAIL ""
ENV GITLAB_TOKEN ""


##################################################
#           install dependecies                  #
##################################################
RUN apt-get update && apt-get install -y sudo wget git build-essential nano unzip curl sudo apt-utils
RUN apt-get update && apt-get install -y g++ software-properties-common apt-transport-https curl gnupg 
RUN apt-get install -y  xvfb x11-utils novnc xserver-xephyr tigervnc-standalone-server  gnumeric jq 

##################################################
#              Terminal Theme                    #
##################################################

RUN /usr/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)" --prefix=/usr/local
RUN cp /usr/local/share/oh-my-bash/bashrc ~/.bashrc
RUN sed -i 's/OSH_THEME="[^"]*"/OSH_THEME="pure"/' ~/.bashrc

##################################################
#           install conda                        #
##################################################
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
RUN bash miniconda.sh -b -p /miniconda
RUN rm miniconda.sh
ENV PATH="/miniconda/bin:${PATH}"
RUN echo 'export PATH="/miniconda/bin:${PATH}"' >> ~/.bashrc
RUN /miniconda/bin/conda config --set auto_activate_base true
RUN /miniconda/bin/conda init
RUN /miniconda/bin/conda run -n base pip install twine astunparse bashi pyvirtualdisplay pyngrok grpcio



##################################################
#               install lua                      #
##################################################
RUN apt-get install -y  lua5.3 



##################################################
#               install npm                      #
##################################################
RUN apt-get install -y  npm



##################################################
#                install Defold                  #
##################################################
RUN wget https://d.defold.com/archive/editor-alpha/553487eb9c84a23b6ddd2afcba0ff6085784a5fa/editor-alpha/editor2/Defold-x86_64-linux.zip
RUN unzip Defold-x86_64-linux.zip 
RUN rm Defold-x86_64-linux.zip 
RUN chmod +x Defold/Defold


##################################################
#             install kasmvncserver              #
##################################################
RUN apt --fix-broken install
RUN wget http://ftp.de.debian.org/debian/pool/main/libw/libwebp/libwebp7_1.2.4-0.2+deb12u1_amd64.deb
RUN sudo dpkg -i libwebp7_1.2.4-0.2+deb12u1_amd64.deb 
RUN sudo apt-get install -f
RUN rm libwebp7_1.2.4-0.2+deb12u1_amd64.deb 
RUN apt --fix-broken install
RUN apt install -y libwebp7 ssl-cert libswitch-perl libyaml-tiny-perl  libdatetime-timezone-perl liblist-moreutils-perl libhash-merge-simple-perl
RUN wget https://github.com/kasmtech/KasmVNC/releases/download/v1.3.0/kasmvncserver_bookworm_1.3.0_amd64.deb
RUN sudo dpkg -i *.deb
RUN rm kasmvncserver_bookworm_1.3.0_amd64.deb

##################################################
#              install ngrok                     #
##################################################
RUN curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc \
  | sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" \
  | sudo tee /etc/apt/sources.list.d/ngrok.list && sudo apt update && sudo apt install ngrok

##################################################
#              install code-server               #
##################################################
RUN export VSCODE_SERVER_VERSION=4.21.1 && curl -fOL https://github.com/coder/code-server/releases/download/v$VSCODE_SERVER_VERSION/code-server_${VSCODE_SERVER_VERSION}_amd64.deb && sudo dpkg -i code-server_${VSCODE_SERVER_VERSION}_amd64.deb && rm code-server_${VSCODE_SERVER_VERSION}_amd64.deb



##################################################
#                  CMD                           #
##################################################
