

###########################################################################
#           ---------------- GITLAAB SET UP ----------------
###########################################################################
git config --global user.name $GIT_USER_NAME
git config --global user.email $GIT_USER_EMAIL
git config --global credential.helper store # This line ensures the token is stored securely
git config --global github.token gitlab.token $GITLAB_TOKEN

